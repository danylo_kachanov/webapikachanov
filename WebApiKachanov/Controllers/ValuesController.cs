﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiKachanov.Helpers;

namespace WebApiKachanov.Controllers
{
    public class ValuesController : ApiController
    {
        [Route("csvdata")]
        public async Task<HttpResponseMessage> GetCsvData()
        {
            var response = Request.CreateResponse();

            response.Content = new StringContent(await DataHelper.ReadCsvFile(response), Encoding.UTF8, "text/html");

            return response;
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
