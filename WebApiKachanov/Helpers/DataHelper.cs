﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace WebApiKachanov.Helpers
{
    public static class DataHelper
    {
        public static async Task<string> ReadCsvFile(HttpResponseMessage response)
        {
            var result = string.Empty;

            try
            {
                var filePath = HttpContext.Current.Request.MapPath(@"~\App_Data\csvdata.txt");

                using (StreamReader sr = new StreamReader(filePath))
                {
                    var line = await sr.ReadToEndAsync();
                    result = line;
                    response.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                result = "Could not read the file";
                response.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }
    }
}